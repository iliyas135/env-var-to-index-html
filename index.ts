#!/usr/bin/env node
import * as fs from "fs-extra";

if (!fs.existsSync("./index.html"))
  throw new Error("No index.html file found in current directory");

let indexHtmlContent = fs.readFileSync("./index.html").toString();

if (indexHtmlContent.indexOf("<head>") === -1)
  throw new Error(
    "No <head> element found in index.html. Please add one since environment variables will be added straight afterwards!"
  );

const args = process.argv;
args.splice(0, 2);
if (args.length === 0)
  throw new Error(
    "Please provide environment variables like this: javascriptVariableName=ENVIRONMENT_VARIABLE_NAME"
  );

const envs = args.map((arg) => {
  if (arg.indexOf("=") === -1)
    throw new Error(
      "Please provide environment variables like this: javascriptVariableName=ENVIRONMENT_VARIABLE_NAME, got " +
        arg
    );
  return arg.split("=");
});

if (
  indexHtmlContent.includes("\n\t\t<!--EVTI_START-->") &&
  indexHtmlContent.includes("<!--EVTI_END-->")
) {
  const startStuff = indexHtmlContent.substring(
    0,
    indexHtmlContent.indexOf("\n\t\t<!--EVTI_START-->")
  );
  const endStuff = indexHtmlContent.substring(
    indexHtmlContent.indexOf("<!--EVTI_END-->") + "<!--EVTI_END-->".length
  );
  indexHtmlContent = startStuff + endStuff;
}

let allEnvs = "<head>\n\t\t<!--EVTI_START-->\n\t\t<script>";
console.log("Updating File: ./index.html with these variables:");
for (const env of envs) {
  if (process.env[env[1]]) {
    allEnvs += `\n\t\t\tconst ${env[0]} = "${process.env[env[1]]}";`;
    console.log(`  |-- const ${env[0]} = "${process.env[env[1]]}";`);
  } else console.log(`  |-- Skipped ${env[0]} since  ${env[1]} is not set!`);
}
allEnvs += "\n\t\t</script>";
allEnvs += "\n\t\t<!--EVTI_END-->";

indexHtmlContent = indexHtmlContent.replace("<head>", allEnvs);

fs.writeFileSync("./index.html", indexHtmlContent, { encoding: "utf-8" });
