#!/usr/bin/env node
"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs-extra"));
if (!fs.existsSync("./index.html"))
    throw new Error("No index.html file found in current directory");
let indexHtmlContent = fs.readFileSync("./index.html").toString();
if (indexHtmlContent.indexOf("<head>") === -1)
    throw new Error("No <head> element found in index.html. Please add one since environment variables will be added straight afterwards!");
const args = process.argv;
args.splice(0, 2);
if (args.length === 0)
    throw new Error("Please provide environment variables like this: javascriptVariableName=ENVIRONMENT_VARIABLE_NAME");
const envs = args.map((arg) => {
    if (arg.indexOf("=") === -1)
        throw new Error("Please provide environment variables like this: javascriptVariableName=ENVIRONMENT_VARIABLE_NAME, got " +
            arg);
    return arg.split("=");
});
if (indexHtmlContent.includes("\n\t\t<!--EVTI_START-->") &&
    indexHtmlContent.includes("<!--EVTI_END-->")) {
    const startStuff = indexHtmlContent.substring(0, indexHtmlContent.indexOf("\n\t\t<!--EVTI_START-->"));
    const endStuff = indexHtmlContent.substring(indexHtmlContent.indexOf("<!--EVTI_END-->") + "<!--EVTI_END-->".length);
    indexHtmlContent = startStuff + endStuff;
}
let allEnvs = "<head>\n\t\t<!--EVTI_START-->\n\t\t<script>";
console.log("Updating File: ./index.html with these variables:");
for (const env of envs) {
    if (process.env[env[1]]) {
        allEnvs += `\n\t\t\tconst ${env[0]} = "${process.env[env[1]]}";`;
        console.log(`  |-- const ${env[0]} = "${process.env[env[1]]}";`);
    }
    else
        console.log(`  |-- Skipped ${env[0]} since  ${env[1]} is not set!`);
}
allEnvs += "\n\t\t</script>";
allEnvs += "\n\t\t<!--EVTI_END-->";
indexHtmlContent = indexHtmlContent.replace("<head>", allEnvs);
fs.writeFileSync("./index.html", indexHtmlContent, { encoding: "utf-8" });
