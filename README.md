# Environment Variables to index.html - EVTI

Adds real environment variables to an index.html file, e.g. for passing dynamic env variables to node applications

## Usage
```
evti javascriptVariableName=ENVIRONMENT_VARIABLE_NAME
```

### Example
```
export API_URL=https://api.smallstack.com
evti apiUrl=API_URL
```

## Getting started
1. Install the package: 

```
npm i -g @smallstack/evti
```

2. Navigate to the folder where your index.html file is located
3. Make sure, all environment variables are set
4. Execute the CLI:

```
evti javascriptVariableName=ENVIRONMENT_VARIABLE_NAME
```


## Screenshots
![image](https://smallstack-cloud-storage.s3.eu-central-1.amazonaws.com/5de1718bbf9f51000e0cdbf5/5f98014dc99829001a202251/62b30fa66faf435f389a32b6)

![image](https://smallstack-cloud-storage.s3.eu-central-1.amazonaws.com/5de1718bbf9f51000e0cdbf5/5f98014dc99829001a202251/62b30fab6faf4317199a330e)

